                            Neural Network

                    TP / COURS de Mr Fabien TEYTAUD


Prérequis :

    pip install networkx

    pip install logging

    pip install pyjavaproperties

    pip install prettytable




Howto

Dans le run.py, à la commande main(A,B),

 Paramètre A
 
    - 'Cours' pour faire fonctionner l'exemple de cours. On doit avoir une sortie de 0.63
    
    - 'Autre' pour passer sur un fichier généré aléatoirement.
    
 Paramètre B
 
    - logging.ERROR pour ne pas logger vers le fichier local
    
    - logging.INFO pour logger vers le fichier activity.log
    
    

B.  Les paramètres de l'application sont dans le fichier neuralnetwork.properties.

La plupart des paramètres sont utilisés pour de l'affichage, mais les premières lignes peuvent utilisées pour jouer avec le réseau :



    Pour changer le nombre d'itérations, IT=1
    
    Pour modifier la valeur du coéfficient d'apprentissage, ALPHA=1
    
    Pour modifier la taille des données d'apprentissage TAILLE_APPRENTISSAGE=100


