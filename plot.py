# -*- coding:Latin-1 -*-
import sys
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
from math import exp
from matplotlib.font_manager import FontProperties

#
# Classe qui permet d'avoir un rendu des points de notre apprentissage
# et le r�sultat graphique du perceptron
#

def maindatas(lesLignes,tableau,alpha):

    xclasse1 = []
    yclasse1 = []
    xclasse2 = []
    yclasse2 = []

    for i in range(len(lesLignes)):

        if int(lesLignes[i][2]) > 0:
            xclasse1.append(float(lesLignes[i][0]))
            yclasse1.append(float(lesLignes[i][1]))
        else:
            xclasse2.append(float(lesLignes[i][0]))
            yclasse2.append(float(lesLignes[i][1]))


    fig = plt.figure()
    plt.plot(xclasse1, yclasse1, 'bo', xclasse2, yclasse2, 'rx')

    x1 = np.linspace(0, 1, 100)

    multiplecinq = len(tableau)
    if multiplecinq>5:
        multiplecinq = len(tableau)/5
    else:
        multiplecinq = 2

    # On trace les courbes avec le tour dans lequel on se trouve ainsi que le nombre d'erreurs
    # (On doit donc en condition normale constater une baisse.)
    # ainsi qu'une l�gende
    x = np.arange(1)
    ax = plt.subplot(111)
    for l in range(0,len(tableau),multiplecinq):
        plt.plot(tableau[l][0], sigmoide(alpha,tableau[l][1]))
        ax.plot(tableau[l][0],  sigmoide(alpha,tableau[l][1]), label="Tour "+ str(l).zfill(4) + "\t\tErr "+str(int(tableau[l][2])))

    # On trace la courbe finale et sa l�gende
    #bias,w1,w2 = n[0],n[2][0],n[2][1]
    #plt.plot(x1, -(w1/w2)*x1 - (bias/w2) + 2)
    #ax.plot(x, tableau[-1][0] * x + tableau[-1][1], label=" Final    \t\tErr " + str(int(tableau[-1][2])))
    #ax.legend(bbox_to_anchor=(1.1, 1.05))

    plt.show()

def sigmoide(alpha,sigmai):
    res,rsint = 0,0
    try:
        rsint = exp(-alpha * sigmai)
        res = 1 / (1 + rsint)
    except OverflowError:
        print "Calculation failed! alpha=%d sigma=%d" % (alpha, sigmai)
        raise
    return res