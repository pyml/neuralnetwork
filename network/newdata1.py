tabneurones = [
    (1, 'Neuron 1', [0, 0], [3, 4], 2, 0),
    (2, 'Neuron 2', [0, 0], [3, 4], -2, 0),
    (3, 'Neuron 3', [1, 2], [5, 6], 0, 0),
    (4, 'Neuron 4', [1, 2], [5, 6], 0, 0),
    (5, 'Neuron 5', [3, 4], [7, 7], 0, 0),
    (6, 'Neuron 6', [3, 4], [7, 7], 0, 0),
    (7, 'Neuron 7', [5, 6], [7, 7], 0, 0)]

tabsynapse = [
    (1, 'Synapse 1', 1, 3, 0.5),
    (2, 'Synapse 2', 1, 4, -1),
    (3, 'Synapse 3', 2, 3, 1.5),
    (4, 'Synapse 4', 2, 4, -2),
    (5, 'Synapse 5', 3, 5, -1),
    (6, 'Synapse 6', 3, 6, -1),
    (7, 'Synapse 7', 4, 5, 3),
    (8, 'Synapse 8', 4, 6, -4),
    (9, 'Synapse 9', 5, 7, 1),
    (10, 'Synapse 10', 6, 7, -3)]

tabnestart = [1, 2]
tabneend = [7]