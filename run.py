#-*- coding: utf-8 -*-
import random
import plot
import network.newdata1
import network.data2
import logging
from pyjavaproperties import Properties
from logging.handlers import RotatingFileHandler
from math import exp
from prettytable import PrettyTable

class Neuron:
    def __init__(self, tabneurones, nat):
        self.id,self.nom = tabneurones[nat][0],tabneurones[nat][1]
        self.synapseentree,self.synapsesortie = tabneurones[nat][2],tabneurones[nat][3]
        self.valeur,self.delta = tabneurones[nat][4],tabneurones[nat][5]

    def getValue(self):
        return self.valeur

    def getNeuronId(self):
        return self.id

    def getSynapseEntree(self, ne):
        return (self.synapseentree[0],ne),(self.synapseentree[1],ne)

class Synapse:
    def __init__(self, tabsynapse, nsy):
        self.id,self.name,self.neuronedepart, self.neuronearrivee, self.poids  = \
        tabsynapse[nsy][0],tabsynapse[nsy][1],tabsynapse[nsy][2],tabsynapse[nsy][3],tabsynapse[nsy][4]

class Network():
    def __init__(self,tabneurones, tabsynapse, tabnestart, tabneend):
        self.tableauxneurones = []
        for count in xrange(len(tabneurones)):
            x = Neuron(tabneurones,count)
            x.attr = count
            self.tableauxneurones.append(x)

        self.tableauxsynapse = []
        for count in xrange(len(tabsynapse)):
            x = Synapse(tabsynapse, count)
            x.attr = count
            self.tableauxsynapse.append(x)

        self.neuronesinit = []
        for count in xrange(len(tabnestart)):
            self.neuronesinit.append(tabnestart[count])
        self.neuronesend = []
        for count in xrange(len(tabneend)):
            self.neuronesend.append(tabneend[count])

    def getNeurones(self):
        return self.tableauxneurones

    def getNeurones(self,nne):
        return self.tableauxneurones[nne]

    def setValue(self,nne,value):
        self.tableauxneurones[nne].valeur = value

    def getSynapses(self):
        return self.tableauxsynapse

    def eval_cours(self):
        erreurs = 0
        nombreiteration = int(p['IT'])
        neuralnetwork.affiche(p['INITIAL'])
        neuralnetwork.iteration(1, False)
        neuralnetwork.affiche(p['LOG_IT'] % nombreiteration)
        sigmai = neuralnetwork.tableauxneurones[6].valeur
        if get_prediction(sigmai) != 1:
            erreurs = erreurs + 1
        t = PrettyTable([p['TOUR'], p['SORTIE'], p['ERREURS'], p['VALALPHA'], p['DATALENGTH']])
        t.add_row([1, sigmai, erreurs, p['ALPHA'], 1])
        print t

    def eval(self,donnees):
        nombreiteration = int(p['IT'])
        neuralnetwork.affiche(p['INITIAL'])
        t = PrettyTable([p['TOUR'], p['SORTIE'],p['ERREURS'], p['VALALPHA'], p['DATALENGTH']])
        for j in range(150):
            erreurs = 0
            for i in range(len(donnees)):
                neuralnetwork.tableauxneurones[0].valeur = donnees[i][0]
                neuralnetwork.tableauxneurones[1].valeur = donnees[i][1]
                valeurapprentissage = donnees[i][2]
                neuralnetwork.iteration(1, False)
                sigmai = neuralnetwork.tableauxneurones[6].valeur
                if get_prediction(sigmai) != valeurapprentissage:
                    erreurs = erreurs + 1
            if j%25==0:
                t.add_row([j, sigmai, erreurs, p['ALPHA'], len(donnees)])
        print t
        logger.warning(p['FIN'])


    def affiche(self,message):
        logger.warning(p['WR_TAB_NEURONE'])
        t0 = PrettyTable([ message ])
        print t0

        t = PrettyTable([p['NEURON'], p['N_NAME'], p['N_ENT_SYN'], p['N_OUT_SYN'], p['N_VAL'], p['N_DELTA']])
        for count in range(len(self.tableauxneurones)):
            t.add_row([self.tableauxneurones[count].id, self.tableauxneurones[count].nom,
                       self.tableauxneurones[count].synapseentree, self.tableauxneurones[count].synapsesortie,
                       self.tableauxneurones[count].valeur, self.tableauxneurones[count].delta])

        print t

        logger.warning(p['WR_TAB_SYNAPSE'])

        t2 = PrettyTable([p['SYN'], p['SYN_NAME'], p['SYN_NEU_DEP'], p['SYN_NEU_ARR'], p['SYN_WEIGHT']])
        for count in range(len(self.tableauxsynapse)):
            t2.add_row([self.tableauxsynapse[count].id, self.tableauxsynapse[count].name,
                       self.tableauxsynapse[count].neuronedepart, self.tableauxsynapse[count].neuronearrivee,
                       self.tableauxsynapse[count].poids])

        print t2

    def newValue(self,nne,log):
        fromneurones = []
        for count in xrange(len(self.tableauxsynapse)):
            synapse = self.tableauxsynapse[count]
            if synapse.neuronearrivee == nne:
                # On recherche le neurone de départ
                valeurneurone = 0
                for count2 in xrange(len(self.tableauxneurones)):
                    ntemp = self.tableauxneurones[count2]
                    if ntemp.id == synapse.neuronedepart:
                        valeurneurone = ntemp.valeur
                # on neuronedep neurone départ / poids
                fromneurones.append(float(valeurneurone)*synapse.poids)

        #a = fromneurones[0]
        #b = fromneurones[1]
        sig = sigmoide(float(p['ALPHA']),sum(fromneurones))
        self.tableauxneurones[nne-1].valeur = sig

        # si le neurone est terminal, on set également le nouveau delta ( 1 - Valeur du noeud)
        if nne == self.neuronesend[0]:
            yj = sig
            self.tableauxneurones[nne-1].delta = (float(p['VAL_ATTENDUE']) - yj)*yj*(1-yj)
        return sig

    def newDelta(self,nne,log):
        # On prend la valeur du neurone demandé
        deltanne = self.tableauxneurones[nne].valeur

        # On regarde synapse arrivant au neurone nne, puis on regarde les neurones d'arrivée du synapse
        liste1 = []
        for count in range(len(self.tableauxsynapse)):
            synapsetemp = self.tableauxsynapse[count]
            if synapsetemp.neuronedepart == nne:
                liste1.append((synapsetemp.neuronearrivee,synapsetemp.poids))

        # On récupère le delta du neurone précédent et le poids du synapse entre les deux
        liste2 = []
        for i in range(len(liste1)):
            for count in range(len(self.tableauxneurones)):
                neuronetemp = self.tableauxneurones[count]
                if neuronetemp.id == liste1[i][0]:
                    liste2.append(neuronetemp.delta)

        i,calc = 0,0
        for i in range(len(liste1)):
            calc = calc + (float(liste1[i][1])*float(liste2[i]))
            logger.warning(p['SYN_WEIGHT'],liste1[i][1],p['N_DELTA'],liste2[i])
        logger.warning(p['N_VAL'],deltanne,p['LOG_CALC'],calc)
        resultat = deltanne * ( 1 - deltanne ) * calc
        self.tableauxneurones[nne-1].delta = resultat
        return resultat

    # Mise à jour des poids en fonction du numéro de synapse choisi
    def Update(self, ns, alpha,log):
        # On va chercher
        # La valeur du neurone de départ
        nd = self.tableauxneurones[(self.tableauxsynapse[ns].neuronedepart-1)].valeur
        # la valeur du poids sur la synapse
        poids = self.tableauxsynapse[ns].poids
        # le delta du neurone d'arrivée
        delta = self.tableauxneurones[self.tableauxsynapse[ns].neuronearrivee-1].delta
        # mise à jour du poids
        newpoids = poids + alpha * nd * delta
        self.tableauxsynapse[ns].poids = newpoids
        logger.warning( p['LOG_UPDATE'] % \
                (ns, self.tableauxsynapse[ns].neuronedepart,nd, self.tableauxsynapse[ns].neuronearrivee, poids, newpoids))
        return newpoids

    def run(self,log):
        # Propagation Avant
        logger.warning(p['SEPAR'])
        logger.warning(p['LOG_PRO'])

        Listeneurone = list(set(range(1,len(neuralnetwork.tableauxneurones))) - set(neuralnetwork.neuronesinit))

        for i in xrange(min(Listeneurone), neuralnetwork.neuronesend[0]+1):
            neuralnetwork.newValue(i,log)

        for i in range(0, neuralnetwork.neuronesend[0]):
                logger.warning("Neuron %s, valeur %s, delta %s" % \
                    (i + 1, neuralnetwork.getNeurones(i).valeur, neuralnetwork.getNeurones(i).delta))
        logger.warning("_____________________________")

        logger.warning(p['LOG_RET'])
        # Rétropropagation
        for i in range(6, 0, -1):
            res = neuralnetwork.newDelta(i,log)
            logger.warning(p['LOG_DELTA'] % \
                            (i, res))
            logger.warning("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ")

        logger.warning("_____________________________")
        logger.warning(p['LOG_UPD'])
        # Mise à jour des poids
        for i in range (len(neuralnetwork.tableauxsynapse)):
            neuralnetwork.Update(i, float(p['ALPHA']),log)

    def iteration(self,i,log):
        for i in range(0,i):
            logger.warning(p['LOG_IT'] % \
                                (i))
            neuralnetwork.run(log)


def sigmoide(alpha,sigmai):
    res,rsint = 0,0
    try:
        rsint = exp(-alpha * sigmai)
        res = 1 / (1 + rsint)
    except OverflowError:
        print "Calculation failed! alpha=%d sigma=%d" % (alpha, sigmai)
        raise
    return res

def init():
    logger.warning(p['SEPAR2'])
    logger.warning(p['LOG_TITLE'])
    logger.warning(p['SEPAR2'])
    return  Network(network.data2.tabneurones, network.data2.tabsynapse, network.data2.tabnestart, network.data2.tabneend)


def get_prediction(sigmai):
    if sigmai > 0.5:
        return 1
    else:
        return 0

def fonction(x, y):
    if x+y-1 > 0:
        return 1
    else :
        return 0

def aleatoires(apprentissage):
    # données du perceptron
    datas = []
    for i in range(apprentissage):
        x1 = random.uniform(0, 1)
        x2 = random.uniform(0, 1)
        datas.append([x1, x2, fonction(x1, x2)])
        i += 1
    return datas

def main(Choix,log):
    global p
    global neuralnetwork
    global logger
    global erreurs

    # FICHIER DE PROPERTIES
    p = Properties()
    p.load(open('neuralnetwork.properties'))

    # LOGGER
    logger = logging.getLogger()
    formatter = logging.Formatter(p['LOG_FORMATTER'])
    file_handler = RotatingFileHandler(p['LOG_FILENAME'], 'a', 1000000, 1)
    file_handler.setLevel(log)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    steam_handler = logging.StreamHandler()
    steam_handler.setLevel(log)
    logger.addHandler(steam_handler)

    neuralnetwork = init()
    donnees = aleatoires(int(p['TAILLE_APPRENTISSAGE']))

    if Choix=='Cours':
        neuralnetwork.eval_cours()
    else:
        neuralnetwork.eval(donnees)

main('Autre',logging.ERROR)